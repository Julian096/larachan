import { createApp } from 'vue';
import App from './App.vue';
import { VuesticPlugin } from 'vuestic-ui';
import 'vuestic-ui/dist/vuestic-ui.css';
import axios from 'axios';
import VueAxios from 'vue-axios';
import mitt from 'mitt';

const emitter = mitt();

const app = createApp(App);
app.config.globalProperties.emitter = emitter;
app.use(VuesticPlugin);
app.use(VueAxios, axios);
app.provide('axios', app.config.globalProperties.axios);
app.mount("#app")